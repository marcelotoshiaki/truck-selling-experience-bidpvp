﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardController : MonoBehaviour
{

    [SerializeField] private float angle;
    [SerializeField] [Range (0,1)] private float maxMoveInX;
    [SerializeField] [Range (0,1)] private float maxMoveInY;
    [SerializeField] [Range (0,1)] private float triggerOptionInX;
    [SerializeField] private float velocityX;
    [SerializeField] private float velocityY;
    public GameObject card;
    private Vector3 initialInputMousePosition;
    private Vector3 initialCardPosition;
    private Vector3 initialCardPositionViewport;
    private Animator cardAnimator;

    void Start()
    {
        initialCardPosition = card.transform.position;
        initialCardPositionViewport = Camera.main.ScreenToViewportPoint(initialCardPosition);
        cardAnimator = card.transform.GetChild(0).GetComponent<Animator>();
    }

    void Update()
    {
        Vector3 cardPositionViewPort = Camera.main.ScreenToViewportPoint((card.transform.position));

        if (Input.GetMouseButtonDown(0)){
            initialInputMousePosition = Input.mousePosition;
        }
        
        if (Input.GetMouseButton(0)){
            
            Vector3 moved = Input.mousePosition - initialInputMousePosition + initialCardPosition;
            Vector3 movedViewport = Camera.main.ScreenToViewportPoint(moved);
            
            if (movedViewport.x >= (0.5f + maxMoveInX)) {
                movedViewport = new Vector3(0.5f + maxMoveInX,movedViewport.y,0);
            }
            else if (movedViewport.x <= (0.5f - maxMoveInX)) {
                movedViewport = new Vector3(0.5f - maxMoveInX,movedViewport.y,0);
            }
    
            movedViewport = new Vector3(movedViewport.x,(movedViewport.y - initialCardPositionViewport.y) * maxMoveInY + initialCardPositionViewport.y,movedViewport.z);

            
            float smoothMovementX = cardPositionViewPort.x - movedViewport.x;
            float smoothMovementY = cardPositionViewPort.y - movedViewport.y;
            
            moved = Camera.main.ViewportToScreenPoint(movedViewport);
            Vector3 moveInX = Vector3.MoveTowards(card.transform.position,moved,Mathf.Abs(smoothMovementX * velocityX * Time.deltaTime)); 
            Vector3 moveInY = Vector3.MoveTowards(card.transform.position,moved,Mathf.Abs(smoothMovementY * velocityY * Time.deltaTime)); 
            
            card.transform.position = new Vector3(moveInX.x,moveInY.y,0);
            card.transform.eulerAngles = new Vector3(0,0,(initialCardPositionViewport.x - cardPositionViewPort.x) * (1/maxMoveInX) * angle);

            transform.GetComponentInChildren<Image>().color = Color.yellow;
            cardAnimator.SetBool("isShowing", false);
            
            if (movedViewport.x >= (0.5f + triggerOptionInX)) {
                transform.GetChild(0).GetComponent<Image>().color = Color.green;
                cardAnimator.SetBool("isShowing", true);
            }
            else if (movedViewport.x <= (0.5f - triggerOptionInX)) {
                transform.GetChild(0).GetComponent<Image>().color = Color.red;
                cardAnimator.SetBool("isShowing", true);
            }
        }
        else {
            float smoothMovementX = cardPositionViewPort.x - initialCardPositionViewport.x;
            float smoothMovementY = cardPositionViewPort.y - initialCardPositionViewport.y;

            Vector3 moveInX = Vector3.MoveTowards(card.transform.position,initialCardPosition,Mathf.Abs(smoothMovementX * velocityX * Time.deltaTime)); 
            Vector3 moveInY = Vector3.MoveTowards(card.transform.position,initialCardPosition,Mathf.Abs(smoothMovementY * velocityY * Time.deltaTime)); 
            
            card.transform.position = new Vector3(moveInX.x,moveInY.y,0);
            card.transform.eulerAngles = new Vector3(0,0,(initialCardPositionViewport.x - cardPositionViewPort.x) * (1/maxMoveInX) * angle);

            transform.GetChild(0).GetComponent<Image>().color = Color.yellow;
            cardAnimator.SetBool("isShowing", false);
        }
    }
}
